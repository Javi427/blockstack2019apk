package com.example.blockstack

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import org.blockstack.android.sdk.BlockstackSession
import org.blockstack.android.sdk.Scope
import org.blockstack.android.sdk.UserData
import org.blockstack.android.sdk.toBlockstackConfig
class MainActivity : AppCompatActivity() {

    private var blockSesion: BlockstackSession? = null
    private val TAG = MainActivity::class.java.simpleName

    //------------------------ Metodo principal-------------------------------------------------------
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val scopes = arrayOf(Scope.StoreWrite)
        val config = "https://flamboyant-darwin-d11c17.netlify.com"
            .toBlockstackConfig(scopes)
        blockSesion = BlockstackSession(this@MainActivity,config)
        botonInicio.isEnabled = true

        //------------------------------ ONCLICK SOBRE EL BOTON DE INICIAR SESION------------------------------------
        botonInicio.setOnClickListener{ _: View ->
            blockstackSession().redirectUserToSignIn {
                Log.d(TAG,"Error")
            }
        }

        if( intent?.action == Intent.ACTION_VIEW){
            handleAuthResponse(intent)
        }
    }


    //------------------------------- para regresar los datos -----------------------------------------------------

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        Log.d(TAG, "Nuevo intento")
        if(intent?.action == Intent.ACTION_MAIN){
            val userData = blockstackSession().loadUserData()
            if (userData != null) {
                runOnUiThread {
                    onSignIn(userData)
                }
            }
        }
        else if(intent?.action == Intent.ACTION_VIEW){
            handleAuthResponse(intent)
        }
    }


    //---------------------- funcion que se ejecutara si ya esta logeado --------------------------------------------
    private fun onSignIn(userData : UserData){
        mostrarInformacion.text = "La informacion es ${userData.decentralizedID}"
        botonInicio.isEnabled = false;
    }


    //--------------------- funcion para encargarse de verificar los datos que retorna BlockStack----------------------
    private fun handleAuthResponse(intent : Intent){
        val response = intent.dataString
        Log.d(TAG, "Este es La respuesta ${response}");
        if(response != null){
            val authResponseTokens = response.split('=')
            if( authResponseTokens.size > 1){
                Log.d(TAG,authResponseTokens[1])
                val authResponse = authResponseTokens[1]

                blockstackSession().handlePendingSignIn(authResponse) { userData ->
                    if(userData.hasValue){
                        runOnUiThread{
                            onSignIn(userData.value!!)
                        }
                    }
                }
            }
        }
    }


    //---------------------- funcion para iniciar sesion ------------------------------------------------------------
     fun blockstackSession() : BlockstackSession{
        val session = blockSesion
        if( session != null) return session
        else throw IllegalStateException("No hay sesion Activa")
    }



}
